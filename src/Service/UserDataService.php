<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\UserData;
use DateTime;

/**
 * Description of UserDataService
 *
 * @author Hector
 */
class UserDataService {

    private $em = null;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }

    /**
     * Crear un registro de UserData
     * @param String $country codigo del pais con longitud maxima de 2 caracteres
     * @param String $eventKey codigo del evento con longitud maxima de 6 caracteres
     * @return Integer PK del registor almacenado
     * @throws Exception cuando la longitud de los parametros no corresponden
     */
    public function create($country, $eventKey) {
        $remoteIP = $_SERVER['REMOTE_ADDR'];
        $userAgent = $_SERVER['HTTP_USER_AGENT'];

        if(strlen($country) > 2) {
            throw new \Exception("El código del país no tiene la longitud requerida");
        }

        if(strlen($eventKey) > 6) {
            throw new \Exception("El código del evento no tiene la longitud requerida");
        }
        
        $userData = new UserData();
        $userData->setCreatedAt(new DateTime('now'));
        $userData->setUpdatedAt(new DateTime('now'));
        $userData->setUserIpAddress($remoteIP);
        $userData->setUserAgent($userAgent);
        $userData->setCountryCode($country);
        $userData->setEventKey($eventKey);

        $this->em->persist($userData);
        $this->em->flush();

        return $userData->getId();
    }

}
