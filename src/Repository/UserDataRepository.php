<?php

namespace App\Repository;

use App\Entity\UserData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;


/**
 * @method UserData|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserData|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserData[]    findAll()
 * @method UserData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserDataRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, UserData::class);
    }

    /**
     * @return UserData[]
     */
    public function getUserDataLust($dateFrom): array {
        $entityManager = $this->getEntityManager();

        $dql = 'SELECT ud '
                . 'FROM App\Entity\UserData ud '
                . 'WHERE ud.createdAt > :dateFrom '
                . 'ORDER BY ud.createdAt ASC';

        $query = $entityManager->createQuery($dql);
        $query->setParameter('dateFrom', $dateFrom);

        // returns an array of Product objects
        return $query->getResult();
    }
}
