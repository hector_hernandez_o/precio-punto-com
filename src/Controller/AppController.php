<?php

namespace App\Controller;

use App\Service\UserDataService;
use DateTime;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AppController extends AbstractController {

    /**
     * @Route("/user/data/create", name="userdata_create")
     * @param UserDataCreate $service
     * @return Response
     */
    public function createUserData(UserDataService $service) {
        $udId = 0;
        $error = "";

        try {
            $udId = $service->create('CO', '123456');
        } catch (\Exception $exc) {
            $error = $exc->getMessage();
        }

        $em = $this->getDoctrine()->getManager();
        $userData = $em->getRepository(\App\Entity\UserData::class)->find($udId);

        return $this->render('main/creado.html.twig', [
            'userData' => $userData,
            'error' => $error,
        ]);
    }

    /**
     * Listar los registros de la base de datos
     * @Route("/user/data/list", name="userdata_list")
     */
    public function listUserData(): Response {

        $dateFrom = new DateTime('now');
        $dateFrom->modify("-30 day");

        $em = $this->getDoctrine()->getManager();
        $userData = $em->getRepository(\App\Entity\UserData::class)->getUserDataLust($dateFrom);

        return $this->render('main/list.html.twig', [
                    'userData' => $userData,
        ]);
    }

}
