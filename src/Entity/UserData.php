<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\UserDataRepository;

/**
 * UserData
 *
 * @ORM\Table(name="user_data")
 * @ORM\Entity(repositoryClass=UserDataRepository::class)
 */
class UserData {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="user_ip_address", type="string", length=200, nullable=true)
     */
    private $userIpAddress;

    /**
     * @var string|null
     *
     * @ORM\Column(name="user_agent", type="text", length=0, nullable=true)
     */
    private $userAgent;

    /**
     * @var string|null
     *
     * @ORM\Column(name="country_code", type="string", length=2, nullable=true)
     */
    private $countryCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="event_key", type="string", length=6, nullable=true)
     */
    private $eventKey;

    public function getId(): ?int {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUserIpAddress(): ?string {
        return $this->userIpAddress;
    }

    public function setUserIpAddress(?string $userIpAddress): self {
        $this->userIpAddress = $userIpAddress;

        return $this;
    }

    public function getUserAgent(): ?string {
        return $this->userAgent;
    }

    public function setUserAgent(?string $userAgent): self {
        $this->userAgent = $userAgent;

        return $this;
    }

    public function getCountryCode(): ?string {
        return $this->countryCode;
    }

    public function setCountryCode(?string $countryCode): self {
        $this->countryCode = $countryCode;

        return $this;
    }

    public function getEventKey(): ?string {
        return $this->eventKey;
    }

    public function setEventKey(?string $eventKey): self {
        $this->eventKey = $eventKey;

        return $this;
    }

}
